Let's talk about the problems (or inconveniences) that we sometimes experience.

1) We may want to **verify the quality of the content** of a (long) youtube video - by analysing its text, instead of watching the entire video...

2) We may want to **get the subtitles** of a given movie/Tv series, without needing third-party API's...

3) We may want to **translate** said subtitles into other languages...


**The goal of this project is to address these three particular problems. **

In order to do so, we want to build a simple python library for that purpose. How can we do this?

Lets dive in:

 - Number 1) requires:
    - access to youtube videos (not sure if we need to download it or not)
    - video to audio
    - audio to text
    - (optional) Natural Language Processing in order to determine the most relevant information (this can
    be done using an n-grams analysis. -> This can even be used to make summaries of the text)
    
 - Number 2) requires:
    - video to audio
    - audio to text
    - time annotations on the text
  
 - Number 3) requires:
    - a translation of the text produced by 2)


